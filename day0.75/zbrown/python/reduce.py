#!/usr/bin/env python3

from typing import TypeVar, List, Callable

A = TypeVar('A')
B = TypeVar('B')

def reduce (f: Callable[[B, A], B], z: B, l: List[A]) -> B:
  if len(l) == 0:
    return z

  x, xs = l[0], l[1:]
  return f(reduce(f, z, xs), x)

print ( reduce(lambda sum, x: sum + x, 0, [1,2,3,4,5]) )
print ( reduce(lambda length, _: length + 1, 0, [1,2,3,4,5]) )
print ( reduce(lambda acc, str: str + acc, "", ["Hello", "how", "are", "you", "today"]) )
