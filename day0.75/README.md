# Day 0.75

## Implement `reduce` for Lists using recursion

#### Define `reduce`
```python
def reduce(fn, initial, list):
  # 
```

#### Where
```python
# Sum a list

reduce(lambda sum, x: sum + x, 0, [1, 2, 3, 4, 5])
# ->
# 15
```

#### Where
```python
# Count length of a list

reduce(lambda length, _: length + 1, 0, [1, 2, 3, 4, 5]) 
# ->
# 5
```

#### Where
```python
# Concatenate a list of strings

reduce(lambda acc, str: str + acc, "", ["Hello", "how", "are", "you", "today"])
# ->
# "Hellohowareyoutoday"
```
