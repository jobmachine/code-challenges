# Day 0

## Given:

The following URL links to an image of the chinese flag: [http://flupy.org/data/flags/cn/cn.gif](http://flupy.org/data/flags/cn/cn.gif).
From the structure of the URL, it seems likely that _many_ country flag images exist at the host flupy.org.

## Ask:
Find and download all country flags from flupy.org.
