import string
import itertools
import requests
import os
from ratelimit import limits, sleep_and_retry
import pycountry

permutations = tuple(itertools.permutations(
    string.ascii_uppercase, 2))
permutationlist = list()
countrylist = list()


@sleep_and_retry
@limits(calls=10, period=1)
def is_downloadable(url):
    """
    Does the url contain a downloadable resource
    """
    try:
      h = requests.head(url, allow_redirects=True, timeout=0.1)
      header = h.headers
      content_type = header.get('content-type')
      if 'image/gif' in content_type.lower():
        return True
        pass
      else:
        return False
    except requests.exceptions.Timeout:
      return False


def createvalidlistofcountrycodes():
    for combo in permutations:
      permutationlist.append(combo[0]+combo[1])


def dothefuckingthing(permutationlist):
  for permutation in permutationlist:
    countrycode = pycountry.countries.get(alpha_2=permutation)
    if countrycode != None:
      FlagURL = "http://flupy.org/data/flags/{0}/{0}.gif".format(
          permutation.lower())
      if is_downloadable(FlagURL) == True:
        r = requests.get(FlagURL, allow_redirects=True, timeout=1)
        open(permutation.lower()+".gif", 'wb').write(r.content)
      else: pass
    else:
      pass


createvalidlistofcountrycodes()
dothefuckingthing(permutationlist)

