{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Control.Concurrent.Async
import qualified Data.ByteString               as BS
import qualified Data.Text                     as T
import           Lib                            ( CountryCode
                                                , countryCodes
                                                )
import           Network.HTTP.Req

getUrl :: CountryCode -> Url Http
getUrl c = http "flupy.org" /: "data" /: "flags" /: c /: (c <> ".gif")

getSavePath :: CountryCode -> FilePath
getSavePath c = "imgs/" <> T.unpack c <> ".gif"

requestImage c = req GET (getUrl c) NoReqBody bsResponse mempty

write :: CountryCode -> BS.ByteString -> IO ()
write = BS.writeFile . getSavePath

downloadFile :: CountryCode -> IO ()
downloadFile c =
  runReq defaultHttpConfig (requestImage c) >>= write c . responseBody

main :: IO [()]
main = mapConcurrently downloadFile countryCodes
