#!/usr/bin/env python3

# long and unnecessary.

import pycountry
import requests
import shutil
from datetime import datetime as dt
from datetime import timedelta
from typing import List
import time


def check_combo_and_download(combos: List[str] = None):
  if combos is None:
    combos = []
  temp_list = []
  while len(combos) > 0:
    for combo in combos:
      print(f'Checking {combo}')
      r = requests.get(f'http://flupy.org/data/flags/{combo}/{combo}.gif')
      if r.status_code == 200:
        print(f'{combo} found.  Writing.\n')
        with open(f'/Users/stevechapman/Documents/flags/{combo}.gif', 'wb') as f:
          r.raw.decode_content = True
          shutil.copyfileobj(r.raw, f)
      elif r.status_code == 429:
        print(f'RATE LIMIT for {combo} - will retry.')
        temp_list.append(combo)
      else:
        print(f'{combo} {r.status_code} - {r.reason}.  Skipping.\n')
      combos = temp_list


def build_other_combos(combos: List[str]):
  other_combos = []
  alphabet = 'abcdefghijklmnopqrstuvwxyz'
  for first_letter in alphabet:
    for second_letter in alphabet:
      if f'{first_letter}{second_letter}' not in combos:
        other_combos.append(f'{first_letter}{second_letter}')
  return other_combos


def main():
  start = dt.now()
  country_list = [country.alpha_2.lower() for country in pycountry.countries]
  other_combos = build_other_combos(country_list)
  print('\n\n*************************\n'
        f'Checking countries from pycountry ({len(country_list)} entries).')
  time.sleep(5)
  check_combo_and_download(country_list)
  step_1 = dt.now()
  print('\n\n*************************\n'
        f'Checking other combos for shits and giggles. ({len(other_combos)} entries).')
  time.sleep(5)
  check_combo_and_download(other_combos)
  end = dt.now()
  print(f'Start: {start}\nEnd: {end}\nDiff: {end - start -  timedelta(0,10)}\n')
  print(f'Step1 (countries): {start}\nEnd: {step_1}\nDiff: {step_1 - start - timedelta(0,5)}\n')
  print(f'Step2 (non-countries): {step_1}\nEnd: {end}\nDiff: {end - step_1 - timedelta(0,5)}\n')


if __name__ == '__main__':
  main()
