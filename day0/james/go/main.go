package main

import (
	"github.com/pariz/gountries"
	"strings"
	"os"
	"net/http"
	"fmt"
	"io"
	"runtime"
	"sync"
)

func ensureDir(dirName string) error {

    err := os.Mkdir(dirName, 0777)

    if err == nil || os.IsExist(err) {
        return nil
    } else {
        return err
    }
}

func getDatFlag(cc string, url string, wg *sync.WaitGroup) error {
	defer wg.Done()
	tgtUrl := strings.Replace(url, "@@", cc, -1)
	filename := "results/" + cc + ".gif"
	fmt.Println("Reaching out to url:", tgtUrl)

	resp, err := http.Get(tgtUrl)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Create the file
    out, err := os.Create(filename)
    if err != nil {
        return err
    }
    defer out.Close()

    // Write the body to file
    _, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return err
}

func main() {
	runtime.GOMAXPROCS(10)
	var wg sync.WaitGroup
	query := gountries.New()
	const flupyUrl = "http://flupy.org/data/flags/@@/@@.gif"
	ensureDir("results")
    wg.Add(len(query.FindAllCountries()))
	for _, country := range query.FindAllCountries() {
		cc := strings.ToLower(country.Codes.Alpha2)
		go getDatFlag(cc, flupyUrl, &wg)
	}
	wg.Wait()

}