# Day 1

## Overview
1. Create a `List` data structure without using any built-in language constructs for Lists (Array, Collection, Dict, etc)
2. Implement `map` for your list.
  ```javascript
  // Functional
  map(s => s.length, list("hello", "how", "are", "you", "today"))
  // -> list(5, 3, 3, 3, 5)
  
  // Object oriented
  myList = new List("hello", "how", "are", "you", "today")
  myList.map(s => s.length)
  // -> List(5, 3, 3, 3, 5) 
  ```
3. Implement `reduce` for your list.
  ```javascript
  // Functional
  reduce((acc, s) => s + acc, "", list("hello", "how", "are", "you", "today"))
  // -> "hellohowareyoutoday"
  
  // Object oriented
  myList = new List("hello", "how", "are", "you", "today")
  myList.reduce((acc, s) => s + acc, "")
  // -> "hellohowareyoutoday"
  ```
4. (Bonus) Implement `map` in terms of `reduce`.


No built-ins!!

* [Functional Specification](#functional-specification)
* [Object Oriented Specification](#object-oriented-specification)

# Functional Specification
## 1. Construct a `List` data structure _without_ using any built-in language constructs for Lists (or Arrays, or Collections, or what have you).
* Your `List` should be polymorphic in its child element type. In other words, your `List` should be able to contain any type of term.
* Your `List` should be homogenous in its child element type. In other words, your `List` should not contain elements of mixed types.

Examples: (Built-in array syntax used for illustration purposes — don't use in practice)

CORRECT:
```haskell
[1,2,3,4,5]
```
```haskell
["hello", "how", "are", "you", "today"]
```
```haskell
[True, False, True, False, False]
```

INCORRECT:
```haskell
[1, "hello", True]
```
This `List` is heterogenous.

## 2. Define a `map` function which operates on your `List` data structure.

### Signature of `map`
`map` should have the following signature:
```haskell
map :: (a -> b) -> List a -> List b
```

Where `a` and `b` denote polymorphic terms.

#### Explanation of function arguments

Lets break apart that signature.

##### `(a -> b)`
Represents a function which accepts some type `a` and returns type `b`.
A reified version of this might look like
```haskell
(Int -> Bool)
```
Which denotes a function that acccepts an argument of type `Int` and returns a term of type `Bool`.

Consider a function which accepts an integer as input and returns whether or not it is even.
```javascript
function isEven(x) {
  return x % 2 == 0
}
```
This function has the type `(Int -> Bool)`. It accepts an integer and returns a boolean.
        
##### `List a`
The list over which we are mapping the function `(a -> b)`. **The types in each argument must MATCH**. This means the `a` in the first argument `(a -> b)` MUST be the same `a` in the second argument `List a`. The justification for this should hopefully be obvious — our mapping function should not be able to be given to a `List` whose elements are incompatible with the mapping function.

##### `List b`
Our return type. You guessed it — it should be the same `b` described by our mapping function `(a -> b)`.

#### All together now
```haskell
map :: (a -> b) -> List a -> List b
```
For all `a` and `b` (`∀ a b.`), given a function from `a` to `b`, and `List a`, return a `List b`.

Reified example:

Given a function `isEven` of type `(Int -> Bool)`, the reified signature of `map` looks like:
```haskell
map :: (Int -> Bool) -> List Int -> List Bool
```

#### Example usage
```javascript
funciton isEven(x) {
  return x % 2 == 0
}

let myList = [1, 2, 3, 4, 5]

map(isEven, myList)
// -> Returns
// [False, True, False, True, False]
```

## 3. Define a `reduce` function which operates on your `List` data structure.

### Signature of `reduce`
```haskell
reduce :: (b -> a -> b) -> b -> List a -> b
```

`reduce` takes `3` arguments.

1. The two argument function `(b -> a -> b)`
    * Accepts terms of type `b` and `a` and returns a term of type `b`.
        * In this context, `b` is typically said to represent an `accumulator`. In other words, it represents the _current state_ of the reduction in present iteration step.
        * In this context, `a` represents the current value relative to the present iteration step.
2. The initial value of the reduction, a term of type `b`.
3. The `List` over which we are performing the reduction.

As noted previously in the `map` instructions, once parameter types are specified, they must be the same throughout the rest of the function application.

#### Example usage
##### Summing a `List` of values:
```javascript
let myList = [1, 2, 3, 4, 5]

let listSum = reduce(function(acc, currentValue) {
  return acc + currentValue
}, 0, myList)
// -> Returns
15
```

The reified type signature in the above instantiation looks like the following
```haskell
reduce :: (Int -> Int -> Int) -> Int -> List Int -> Int
```
> (Note, it's perfectly okay for disparate polymorphic type variables to be reified as the same type. In this case both `a` and `b` were reified as type `Int`).

Same example with reducer broken out into a discrete function (instead of the anonymous lambda function):
```javascript
let myList = [1, 2, 3, 4, 5]

function sumReducer(acc, currentValue) {
  return acc + currentValue 
}

let listSum = reduce(sumReducer, 0, myList)
// -> Returns
15
```

##### Concatenating a `List` of strings:

```javascript
let myList = ["Hello", "how", "are", "you"]

function concatReducer(acc, currentValue) {
  return currentValue + acc
}

let concatenatedString = reduce(concatReducer, "", myList)
// -> Returns
"Hellohowareyou"
```

Reified type signature of the above
```haskell
reduce :: (String -> String -> String) -> String -> List String -> String
```

##### Calculating the length of a `List`

```javascript
let myList = ["Hello", "how", "are", "you"]

reduce(function(length, _) { // currentValue is unecessary for determining length, we denote as `_` (unused parameter)
  return length + 1
}, 0, myList)
// -> Returns
4
```

Reified type signature of the above
```haskell
reduce :: (Int -> String -> Int) -> Int -> List String -> Int
```
(Notice how this lines up with our polymorphic signature)
```haskell
reduce :: (b -> a -> b) -> b -> List a -> b
```

## 4. (Bonus!!!!) Define `map` in terms of `reduce`.

`map` can be implemented using `reduce`. Try to work this out.

# Object Oriented Specification

Implement the following interface
```typescript
interface List<A> {
  map<B>(mapItem: (item: A) => B): List<B>;
  reduce<B>(reducer: (accumulator: B, currentItem: A) => B, initialValue: B): B;
}
```

Let's go over the details starting with the interface declaration:
### Interface declaration
```typescript
interface List<A> {
```
`A` is a polymorphic type parameter. It means the `List` class can contain any type of element and _every_ element must be of the same type (homogenous polymorphism).

### `Map`
```typescript
  map<B>(mapItem: (item: A) => B): List<B>
```

The `map` function is an example of a construct called a "higher-order function". Simply, it's a function which accepts another function as a parameter.

In this case, it is given a function (`mapItem`) which is executed upon every element inside the `List` as a means to transform existing elements in the list into a new `List`.

`map` should always be length preserving — the length of the resulting list should be the same length as the original list.

Example

```typescript
let myList = new List(1, 2, 3, 4, 5)
let isEvenList = myList.map(x => x % 2 == 0)
// -> List(false, true, false, true, false)
```

### `Reduce`

```typescript
  reduce<B>(reducer: (accumulator: B, currentItem: A) => B, initialValue: B): B
```
Like `map`, `reduce` is a higher-order function.

Reduce is used to reduce a sequence of values (`List<A>`) to a single value (`B`).

The first parameter (`reducer`) is a two-argument function. This function will be executed upon _every_ element in the `List`, 
where `accumulator` denotes the current value in the reduction process, and `currentItem` denotes the current list element in the present iteration step.

The second parameter `initialValue` is, you guessed it, the starting value used in the reduction process. This value will be given as the `accumulator`
parameter in the `reducer` function on initial iteration.

Example
```typescript
let myList = new List(1, 2, 3, 4, 5)
myList.reduce((sum, currentValue) => sum + currentValue, 0)
// -> 15
// (0 + 1 + 2 + 3 + 4 + 5)
```
