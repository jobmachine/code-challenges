import { flip, compose } from "./helpers";

type Nil = { _tag: "Nil" };
type Cons<A> = { _tag: "Cons"; value: A; tail: List<A> };
export type List<A> = Nil | Cons<A>;

export const Cons = <A>(value: A) => (tail: List<A>): List<A> => ({
  _tag: "Cons",
  value,
  tail
});
export const Nil = <A>(): List<A> => ({ _tag: "Nil" });

export const map = <A, B>(f: (a: A) => B) => (as: List<A>): List<B> => {
  switch (as._tag) {
    case "Nil":
      return Nil();
    case "Cons":
      const { value, tail } = as;
      return Cons(f(value))(map(f)(tail));
  }
};

export const reduce = <A, B>(f: (b: B) => (a: A) => B, initial: B) => (
  as: List<A>
): B => {
  switch (as._tag) {
    case "Nil":
      return initial;
    case "Cons":
      const { value, tail } = as;
      return f(reduce(f, initial)(tail))(value);
  }
};

// `map` in terms of `reduce`
export const mapPrime = <A, B>(f: (a: A) => B) =>
  reduce(flip(compose(Cons)(f)), Nil());
