export const flip = <A, B, C>(f: (a: A) => (b: B) => C) => (b: B) => (a: A) =>
  f(a)(b);
export const compose = <B, C>(g: (b: B) => C) => <A>(f: (a: A) => B) => (
  a: A
) => g(f(a));
