import { map, reduce, mapPrime } from "./List";
import { mkList } from "./sugar";

describe("list", () => {
  describe("map", () => {
    it("correctly maps strings to their lengths", () => {
      const stringList = mkList("Hello", "how", "are", "you");

      const stringLength: (s: string) => number = s => s.length;

      expect(map(stringLength)(stringList)).toEqual(mkList(5, 3, 3, 3));
    });

    it("correctly maps the numbers to is even", () => {
      const intList = mkList(1, 2, 3, 4);

      const isEven: (x: number) => boolean = x => x % 2 === 0;
      expect(map(isEven)(intList)).toEqual(mkList(false, true, false, true));
    });
  });

  describe("reduce", () => {
    it("correctly concatenates a list of strings", () => {
      const stringList = mkList("Hello", "how", "are", "you");

      expect(reduce(acc => s => s + acc, "")(stringList)).toEqual(
        "Hellohowareyou"
      );
    });

    it("correctly counts the length of the list", () => {
      const stringList = mkList("Hello", "how", "are", "you");

      expect(reduce(acc => _ => acc + 1, 0)(stringList)).toEqual(4);
    });
  });

  describe("mapPrime", () => {
    it("correctly maps strings to their lengths", () => {
      const stringList = mkList("Hello", "how", "are", "you");

      const stringLength: (s: string) => number = s => s.length;

      expect(mapPrime(stringLength)(stringList)).toEqual(mkList(5, 3, 3, 3));
    });

    it("correctly maps the numbers to is even", () => {
      const intList = mkList(1, 2, 3, 4);

      const isEven: (x: number) => boolean = x => x % 2 === 0;

      expect(mapPrime(isEven)(intList)).toEqual(
        mkList(false, true, false, true)
      );
    });
  });
});
