import { List, Cons, Nil } from "./List";

// Syntactic sugar over List construction
// Continuously apply `Cons` over arguments
export const mkList = <A>(...as: A[]): List<A> =>
  as.reverse().reduce((as, a) => Cons(a)(as), Nil() as List<A>);
