module Main where

data List a = Nil | Cons a (List a) deriving (Show, Eq)

map' :: (a -> b) -> List a -> List b
map' _ Nil         = Nil
map' f (Cons a as) = Cons (f a) (map' f as)

reduce :: (b -> a -> b) -> b -> List a -> b
reduce _ b Nil         = b
reduce f b (Cons a as) = f (reduce f b as) a

map'' :: (a -> b) -> List a -> List b
map'' f = reduce (flip (Cons . f)) Nil

-- Test data
list :: List Int
list = Cons 3 (Cons 5 (Cons 10 (Cons 10 Nil)))

main :: IO ()
main = do
  print $ map' (+1) list

  print $ reduce (+) 0 list

  print $ map'' (+1) list

  print $ map' (+1) list == map'' (+1) list
