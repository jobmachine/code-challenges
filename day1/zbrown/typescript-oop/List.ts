const END_OF_LIST = "__END_OF_LIST__";

type ListItem<A> = { value: A; tail: List<A> };
type EndOfList = { value: typeof END_OF_LIST };
type ListData<A> = ListItem<A> | EndOfList;

class List<A> {
  private data: ListData<A>;

  constructor(value: typeof END_OF_LIST);
  constructor(value: A, tail: List<A>);
  constructor(value: A | typeof END_OF_LIST, tail?: List<A>) {
    if (value === END_OF_LIST) {
      this.data = { value };
    } else {
      if (tail === undefined) {
        throw new Error(`Tail must be given when not end of list.`);
      }
      this.data = { value, tail };
    }
  }

  private isEndOfList(data: ListData<A>): data is EndOfList {
    return data.value === END_OF_LIST;
  }

  /**
   * Pretty print
   */
  show(): string {
    if (this.isEndOfList(this.data)) {
      return "";
    }

    const { value, tail } = this.data;

    return `${value}, ${tail.show()}`;
  }

  map<B>(f: (a: A) => B): List<B> {
    if (this.isEndOfList(this.data)) {
      return new List<B>(END_OF_LIST);
    }

    const { value, tail } = this.data;

    return new List(f(value), tail.map(f));
  }

  reduce<B>(f: (accumulator: B, currentValue: A) => B, initialValue: B): B {
    if (this.isEndOfList(this.data)) {
      return initialValue;
    }

    const { value, tail } = this.data;
    return f(tail.reduce(f, initialValue), value);
  }

  /**
   * `map` in terms of `reduce`.
   */
  mapPrime<B>(f: (a: A) => B): List<B> {
    return this.reduce(
      (accumulator, currentValue) => new List(f(currentValue), accumulator),
      new List<B>(END_OF_LIST)
    );
  }
}

// Syntactic sugar over creating a list
const mkList = <A>(...items: A[]): List<A> =>
  items
    .reverse()
    .reduce(
      (list, item) => new List(item, list),
      new List(END_OF_LIST) as List<A>
    );

const myList = mkList(1, 2, 3, 4, 5);
console.log(myList.mapPrime(x => x % 2 == 0).show());
