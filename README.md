# JM Coding Challenges


## Hi
Hello, and welcome to Job Machine coding challenges.

Format follows, please read.

## Format
Each unique challenge will be located within a folder at the root of the repository.
Each folder will be named according to the formula: 
`day{n}` where `n` denotes the successor to the natural number used in the previous day.

Each folder will contain its own README file, outlining the instructions of the challenge.

### Contributing
To post your challenge for a given day, create a new folder within the respective challenge folder.
The name of the folder should be your slack handle. If you would like to contribute
solutions in multiple languages, please create multiple folders within your slack handle subfolder denoting
the language.

For example, if my slack handle is `zbrown` and I am contributing my solution to the `day0`
challenge in `php`, `javascript`, and `haskell`, the folder structure should look like the following:
```
.
└── day0
    └── zbrown
        ├── haskell
        ├── javascript
        └── php
```
