# Day 0.5

## Implement `map` for Lists using recursion
(built-ins OK)

#### Given:
```python
x = [1, 2, 3, 4, 5]
```

#### Define `map`
```python
def map(fn, list):
  # 
```

#### Where
```python
def plusOne(n):
  return n + 1

map(plusOne, x)
# ->
# [2, 3, 4, 5, 6]
```

#### Where
```python
def isEven(n):
  return n % 2 == 0

map(isEven, x)
# ->
# [False, True, False, True, False]
```
