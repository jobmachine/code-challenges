#!/usr/bin/env python3

from typing import TypeVar, List, Callable

A = TypeVar('A')
B = TypeVar('B')

x = [1, 2, 3, 4, 5]

def map(f: Callable[[A], B], l: List[A]) -> List[B]:
  x, xs = l[0], l[1:]
  if len(l) == 0:
    return []
  if len(xs) == 0:
    return [f(x)]

  return [f(x)] + map(f, xs)

print( map( lambda n: n+1, x ) )
print( map( lambda n: n%2 == 0, x ) ) 