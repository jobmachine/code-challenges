module Map where

import           Prelude hiding (map)

map :: (a -> b) -> [a] -> [b]
map _ []     = []
map f [a]    = [f a]
map f (a:as) = f a : map f as

main :: IO ()
main = do
  print $ map (1+) [1, 2, 3, 4, 5]
