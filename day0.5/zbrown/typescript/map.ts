const x = [1, 2, 3, 4, 5];

const map = <A, B>(f: (a: A) => B, list: A[]) => {
  switch (list.length) {
    case 0:
      return [];
    case 1: {
      const [a] = list;
      return [f(a)];
    }
    default: {
      const [a, ...as] = list;
      return [f(a), ...map(f, as)];
    }
  }
};

console.log(map(n => n + 1, x));
